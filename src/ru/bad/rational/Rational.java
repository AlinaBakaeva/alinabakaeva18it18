package ru.bad.rational;

public class Rational {
    private int denominator;
    private int divider;


    public Rational(int denominator, int divider) {
        this.denominator = denominator;
        this.divider = divider;
    }

    public Rational() {
        this(1, 1);
    }

    @Override
    public String toString() {
        return "Rational{" +
                "denominator=" + denominator +
                ", divider=" + divider +
                '}';
    }

    Rational mult(Rational rational) {

        int denominator = this.raw().denominator * rational.raw().denominator;
        int deviver = this.raw().divider * rational.raw().divider;
        return new Rational(denominator, deviver).raw();
    }

    Rational div(Rational rational) {

        int denominator = this.raw().denominator * rational.raw().divider;
        int deviver = rational.raw().denominator * this.raw().divider;
        return new Rational(denominator, deviver).raw();
    }

    Rational plus(Rational rational) {
        int denominator = 0;
        int deviver = 0;

        if (this.divider != rational.divider) {
            denominator = this.raw().denominator * rational.raw().divider + rational.raw().denominator * this.raw().divider;
            deviver = rational.raw().divider * this.raw().divider;
        }
        return new Rational(denominator, deviver).raw();
    }

    Rational mines(Rational rational) {
        int denominator = 0;
        int deviver = 0;

        if (this.divider != rational.divider) {
            denominator = this.raw().denominator * rational.raw().divider - rational.raw().denominator * this.raw().divider;
            deviver = rational.raw().divider * this.raw().divider;
        }
        return new Rational(denominator, deviver).raw();
    }

    Rational raw() {
        int a = denominator;
        int b = divider;

        a/= nod(a,b);
        b/= nod(a,b);

        return new Rational(a, b);
    }

    static int nod (int a, int b){
        a=Math.abs(a);
        b=Math.abs(b);
        while (a!=b){
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }
    public int getDenominator() {
        return denominator;
    }

    public int getDivider() {
        return divider;
    }
}


