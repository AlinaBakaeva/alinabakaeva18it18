package ru.bad.song;

public class DemoSong {

    public DemoSong() {

    }

    /**
     * Класс для работы с объектами класса Baggage
     *
     * @author Bakaeva A.
     */

    public static void main(String[] args) {
        Song yesterday  = new Song("Yesterday", "The Beatles", 90);
        Song smell = new Song("Smells Like Teen Spirit", "Nirvana ", 223);
        Song queen = new Song("Dancing Queen", "ABBA", 342);
        Song planes = new Song("Paper Planes", "M.I.A", 51);
        Song sna = new Song("Seven Nation Army", "The White Stripes ", 190);

        Song[] box = {yesterday, smell, queen, planes, sna};
        songs(box);

        /**
         * Цикл, позволяющий вывести на экран схожи ли последние песни или несхожи
         *
         * @param box массив песен
         */

        for (int i = 0; i < box.length; i++) {
            if (box[0].isSameCategory(box[box.length - 1])) {
                System.out.println("Первая и последня песни - схожи");
                break;
            } else {
                System.out.println("Несхожи");
            }
        }
    }
    /**
     * Метод, позволяющий вывести на экран песни категории short
     *
     * @param box массив песен
     */

    private static void songs(Song box[]) {
        int c = Song.getDuration();
        if (c < 120) {
            for (Song song : box) {
                    if (song.category().equals("short")) {
                        System.out.println(song + "" + "short");
                        return;

                    }

                    }
                }
            }
        }






