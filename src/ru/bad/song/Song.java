package ru.bad.song;

/**
 * Класс для представления песни
 *
 * @author Bakaeva A.
 */
@SuppressWarnings("all")

public class Song {

    /**
     * Название песни
     * Автор песни
     * Продолжительность псени(в секундах)
     */

    private String name;
    private String executor;
    private static int duration;

    /**
     * @param name
     * @param executor
     * @param duration
     */

    Song(String name, String executor, int duration) {
        this.name = name;
        this.executor = executor;
        this.duration = duration;
    }


    Song() {
        this("Не указано", "Не указано", 0);
    }

    public String getName() {
        return name;
    }

    public String getExecutor() {
        return executor;
    }

    public static int getDuration() {
        return duration;
    }
    /**
     * Метод, который позволяет определить длитеельность песни
     */
    public String category() {
        String category = new String();
        if (duration <= 120 ) {
            category = "short";
        }
        if (duration > 240 ) {
            category = "long";
        }
        if( duration > 120 && duration <= 240 ){
            category = "medium";
        }
        return category;
    }

    boolean isSameCategory(Song song){
        return this.category().equals(song.category());
    }
    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая название песни, автора, продолжительность
     */
    @Override
    public String toString() {
        return "Song{" +
                "Название='" + name + '\'' +
                ", автор='" + executor + '\'' +
                ", продолжительность=" + duration +
                '}';
    }
}

