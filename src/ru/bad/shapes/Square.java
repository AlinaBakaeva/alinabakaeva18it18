package ru.bad.shapes;

public class Square extends Shape{
    private double side;
    private Point center;

    public Square(Color color, double side, Point center) {
        super(color);
        this.side = side;
        this.center = center;
    }
    public double area() {
        return side * side;
    }

    @Override
    public String toString() {
        return "Square{" + "цвет=" +
                getColor() +
                "side=" + side +
                ", conter=" + center +
                '}';
    }
}

