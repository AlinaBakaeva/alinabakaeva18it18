package ru.bad.shapes;

public class Triangle extends Shape {
    private Point a;
    private Point b;
    private Point c;

    public Triangle(Color color, Point a, Point b, Point c) {
        super(color);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double area(){
        return Math.abs(((a.getX()- c.getX())*(b.getY()-c.getY()))-((b.getX()-c.getX())*(a.getY()-c.getY())))/2;
    }

    @Override
    public String toString() {
        return "Riangle{" + "цвет=" + getColor() +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }
}

