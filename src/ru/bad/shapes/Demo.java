package ru.bad.shapes;

public class Demo {
    public static void main(String[] args) {
        Circle circle = new Circle(Color.BLUE, new Point(), 5);
        Triangle triangle = new Triangle(Color.BLACK, new Point(7, 13), new Point(5, 8), new Point(1, 2));
        Square square = new Square(Color.WHITE, 5, new Point());

        Shape[] shape = {circle, triangle, square};
        double maxShape = MaxShapeArea(shape);

        System.out.println("Максимальная площадь:" + maxShape);
        Name(shape, circle, triangle, square, MaxShapeArea(shape));
    }

    public static double MaxShapeArea(Shape[] shape) {
        double max = shape[0].area();
        for (int i = 0; i < shape.length; i++) {
            double area = shape[i].area();
            if (area > max) {
                max = area;
            }
        }
        return max;
    }


    public static void Name(Shape shape[], Circle circle, Triangle triangle, Square square, double max) {
        if (max == shape[0].area()) {
            System.out.println(circle);
        } else if (max == shape[1].area()) {
            System.out.println(triangle);
        } else if (max == shape[2].area()) {
            System.out.println(square);
        }

    }

}

