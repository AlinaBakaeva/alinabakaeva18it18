package ru.bad.robot;

import java.util.Scanner;

public class Move {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Robot robot = input();
        System.out.println(robot);

        System.out.println("Введите конечную позицию x: ");
        int endX = scanner.nextInt();
        System.out.println("Введите конечную позицию y: ");
        int endY = scanner.nextInt();

        robot.move(endX, endY);
        System.out.println(robot);
    }


    private static Robot input() {
        Direction directionNow = Direction.UP;
        System.out.println("Введите конечную позицию x: ");
        int x = scanner.nextInt();
        System.out.println("Введите конечную позицию y: ");
        int y = scanner.nextInt();
        System.out.println("Введите куда изначально смотрит робот: ");
        scanner.nextLine();
        String lookAt = scanner.nextLine();
        switch (lookAt) {
            case "вправо":
                directionNow = Direction.RIGHT;
                break;
            case "влево":
                directionNow = Direction.LEFT;
                break;
            case "вверх":
                directionNow = Direction.UP;
                break;
            case "вниз":
                directionNow = Direction.DOWN;
                break;
            default:
                break;

        }
        return new Robot(x, y, directionNow);


    }
}

