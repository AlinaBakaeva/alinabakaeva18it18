package ru.bad.robot;

public enum Direction {
    RIGHT,
    LEFT,
    UP,
    DOWN,
}
