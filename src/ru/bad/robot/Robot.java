package ru.bad.robot;

public class Robot {
    private int x;
    private int y;
    private Direction direction;

    Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public Robot(int x, int y) {
        this(x, y, Direction.UP);

    }

    private void turnRight() {
        switch (direction) {
            case UP:
                direction = Direction.RIGHT;
                break;
            case DOWN:
                direction = Direction.DOWN;
                break;
            case LEFT:
                direction = Direction.LEFT;
                break;
            case RIGHT:
                direction = Direction.RIGHT;
                break;
        }
    }

    void move(int endX, int endY) {
        if (endX > x) {
            while (direction != Direction.RIGHT) {
                turnRight();
            }
            while (x != endX) {
                oneStep();
            }
        }
        if (endX < x) {
            while (direction != Direction.LEFT) {
                turnRight();
            }
            while (x != endX) {
                oneStep();
            }
        }
        if (endY > y) {
            while (direction != Direction.UP) {
                turnRight();
            }
            while (y != endY) {
                oneStep();
            }
        }
        if (endY < y) {
            while (direction != Direction.DOWN) {
                turnRight();
            }
            while (y != endY) {
                oneStep();
            }
        }
    }

    private void oneStep() {
        switch (direction) {
            case RIGHT:
                x++;
                break;
            case LEFT:
                x--;
                break;
            case DOWN:
                y--;
                break;
            case UP:
                y++;
                break;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }
}
