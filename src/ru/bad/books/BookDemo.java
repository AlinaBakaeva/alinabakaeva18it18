package ru.bad.books;

import java.util.ArrayList;

public class BookDemo {
    public static void main(String[] args) {
        Book book1 = new Book("Автор1", "Книга1", 2007);
        Book book2 = new Book("Автор2", "Книга2", 2007);
        Book book3 = new Book("Автор3", "Книга3", 2018);
        Book book4 = new Book("Автор4", "Книга4", 1894);
        Book book5 = new Book("Автор5", "Книга5", 2001);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        printMessageBooks(books);
        printComparison(books);
    }


    private static void printMessageBooks(ArrayList<Book> books) {
        if (books.get(0).isYears(books.get(1))) {
            System.out.println("Первая и вторая книга одного года");
        } else {
            System.out.println("Первая и вторая книга не одного года");
        }
    }




    private static void printComparison(ArrayList<Book> books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println("Книга 2018-го года издания \n" + book);

            }
        }
    }
}



