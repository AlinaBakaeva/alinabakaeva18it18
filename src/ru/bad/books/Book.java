package ru.bad.books;

public class Book {
    private String surname;
    private String name;
    private static int year;

    Book(String surname, String name, int year) {
        this.surname = surname;
        this.name = name;
        this.year = year;
    }

    Book() {
        this("Не указано", "Не указано", 0);
    }

    static int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "(" +
                "Фамилия автора - " + surname + '\'' +
                ", Название книги - " + name + '\'' +
                ", Год издания книги -" + year +
                ')';
    }


    boolean isYears(Book book) {
        return this.year == getYear();
    }


    }



