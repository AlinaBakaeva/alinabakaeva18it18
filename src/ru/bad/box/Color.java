package ru.bad.box;

public enum Color {
    WHITE,
    YELLOW,
    RED,
    BLUE,
    BLACK,
    GREEN
}
