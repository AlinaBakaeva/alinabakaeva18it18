package ru.bad.college;
import java.util.Scanner;
/**Класс для реализации действий с преподавателями и студентами
 *
 * @Autor Bakaeva A.D.
 */
public class Demo {
        public static void main(String[] args) {
            Student student1 = new Student("Жданова", Gender.FEMALE, 2017, "14.54.25");
            Student student2 = new Student("Харитонов", Gender.MALE, 2007, "14.54.25");
            Student student3 = new Student("Лапкина", Gender.FEMALE, 2017, "14.54.25");
            Student student4 = new Student("Стасоа", Gender.MALE, 2009, "14.54.25");
            Student student5 = new Student("Далекова", Gender.FEMALE, 2017, "14.54.25");
            Student student6 = inputStudent();
            Student[] students = {student1, student2, student3, student4, student5, student6};

            System.out.println();

            Teacher teacher1 = new Teacher("Дорофеев", Gender.MALE, "физика", false);
            Teacher teacher2 = new Teacher("Соломинский", Gender.MALE, "физ-ра", false);
            Teacher teacher3 = new Teacher("Стадник", Gender.FEMALE, "английский", true);
            Teacher teacher4 = new Teacher("Пузренков", Gender.MALE, "WS", false);
            Teacher teacher5 = new Teacher("Воробьева", Gender.FEMALE, "матиматика", true);
            Teacher teacher6 = inputTeacher();
            Teacher[] teachers = {teacher1, teacher2, teacher3, teacher4, teacher5, teacher6};

            Person[][] people = {teachers, students};

            System.out.println();
            System.out.print("Количество девушек 2017 года поступления: " + qualityOfGirls(students));

            System.out.println();
            System.out.println("Преподаватели-кураторы: ");
            curators(teachers);
            System.out.println();
            System.out.println("Преподаватели и студеты мужского пола:");
            teachersAndStudentsMale(people);
        }

        /**ввод преподавателя с клавиатуры
         *
         * @return объект с преподавателем
         */
        private static Teacher inputTeacher() {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Фамилия преподавателя: ");
            String surname = scanner.nextLine();
            System.out.print("Пол(м или ж): ");
            String gender = scanner.nextLine();
            Gender gender1 = Gender.FEMALE;
            if (gender.equalsIgnoreCase("м")) {
                gender1 = Gender.MALE;
            }
            if (gender.equalsIgnoreCase("ж")) {
                gender1 = Gender.FEMALE;
            }
            System.out.print("Дисциплина: ");
            String discipline = scanner.nextLine();
            System.out.print("Куратор(да или нет): ");
            String isCurator = scanner.nextLine();
            boolean isCurator1 = false;
            if (isCurator.equalsIgnoreCase("да")) {
                isCurator1 = true;
            }
            if (isCurator.equalsIgnoreCase("нет")) {
                isCurator1 = false;
            }
            return new Teacher(surname, gender1, discipline, isCurator1);
        }


        public static Student inputStudent() {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Фамилия студента: ");
            String surname = scanner.nextLine();
            System.out.print("Пол(м или ж): ");
            String gender = scanner.nextLine();
            System.out.print("Год поступления: ");
            int entrance = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Код специальности: ");
            String codeOfProfession = scanner.nextLine();
            Gender gender1 = Gender.FEMALE;
            if (gender.equalsIgnoreCase("м")) {
                gender1 = Gender.MALE;
            }
            if (gender.equalsIgnoreCase("ж")) {
                gender1 = Gender.FEMALE;
            }
            return new Student(surname, gender1, entrance, codeOfProfession);
        }

        /**нахождение количества студенток с опр. годом поступления
         *
         * @param students массив студентов
         * @return количество студенток
         */
        public static int qualityOfGirls(Student[] students) {
            int quality = 0;
            for (int i = 0; i < students.length; i++) {
                if (students[i].getYear() == 2017) {
                    quality++;
                }
            }
            return quality;
        }

        /**вывод преподавателей кураторов
         *
         * @param teachers массив кураторов
         */
        public static void curators(Teacher[] teachers) {
            for (int i = 0; i < teachers.length; i++) {
                if (teachers[i].isCurator()) {
                    System.out.println(teachers[i] + " ");
                }
            }
        }


        public static void teachersAndStudentsMale(Person[][] people) {
            for (int i = 0; i < people.length; i++) {
                for (int j = 0; j < people[0].length; j++) {
                    if (people[i][j].getGender() == Gender.MALE) {
                        System.out.println(people[i][j] + " ");
                    }
                }
            }
        }
    }


