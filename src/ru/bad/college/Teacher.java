package ru.bad.college;

public class Teacher extends Person{
    private String discipline;
    private boolean curator;

    public Teacher(String name, Gender gender, String discipline, boolean curator) {
        super(name, gender);
        this.curator = curator;
        this.discipline = discipline;
    }

    public String getDiscipline() {
        return discipline;
    }

    public boolean isCurator() {
        return curator;
    }
    @Override
    public String toString() {
        return "(" +
                " Дисциплина - " + discipline + '\'' +
                ", Куратор -" + curator +
                ')';
    }

}
