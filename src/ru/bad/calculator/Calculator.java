package ru.bad.calculator;

import java.util.Scanner;
public class Calculator {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int x;
        int y;
        System.out.print("Введите первое целое число:");
        x = scanner.nextInt();
        System.out.print("Введите второе целое число:");
        y = scanner.nextInt();
        System.out.println("Cумма чисел "+ x + " и " + y + " = " + calculatorMath.MathInt.sum(x,y));
        System.out.println("Разность чисел "+ x + " и " + y + " = " + calculatorMath.MathInt.del(x,y));
        System.out.println("Умножение чисел "+ x + " и " + y + " = " + calculatorMath.MathInt.multiplication(x,y));
        System.out.println("Целочисленное деление чисел "+ x + " и " + y + " = " + calculatorMath.MathInt.divison(x,y));
        System.out.println("Остаток от деления чисел "+ x + " и " + y + " = " + calculatorMath.MathInt.remains(x,y));
        System.out.println("Введение числа "+ x + " в степень " + y + " = " + calculatorMath.MathInt.pow(x,y));
    }
}

