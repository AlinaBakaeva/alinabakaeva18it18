package ru.bad.calculator;

import java.util.Scanner;
public class Calculator2 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int x, y;
        String expression;
        System.out.println("Введите значения с проблеми - ");
        expression = scanner.nextLine();

        String[] variables = expression.split(" ");
        x = Integer.parseInt(variables[0]);
        y = Integer.parseInt(variables[2]);

        switch (variables[1]) {
            case "+":
                System.out.println(expression + "=" + calculatorMath.MathInt.sum(x, y));
            case "-":
                System.out.println(expression + "=" + calculatorMath.MathInt.del(x, y));
            case "*":
                System.out.println(expression + "=" + calculatorMath.MathInt.multiplication(x, y));
            case "/":
                System.out.println(expression + "=" + calculatorMath.MathInt.divison(x, y));
            case "%":
                System.out.println(expression + "=" + calculatorMath.MathInt.remains(x, y));
            case "^":
                System.out.println(expression + "=" + calculatorMath.MathInt.pow(x, y));
        }
    }
}