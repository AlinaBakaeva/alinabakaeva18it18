package ru.bad.calculator;

class calculatorMath {
    static class MathInt {

        static int sum(int x, int y) {
            return (x + y);
        }

        static int del(int x, int y) {
            return (x - y);
        }

        static int multiplication(int x, int y) {
            return (x * y);
        }

        static int divison(int x, int y) {
            return (x / y);
        }

        static int remains(int x, int y) {
            return (x % y);
        }

        static double pow(int x, int y) {
            if (y == 0) {
                return 1;
            }
            double result = x;
            if (y < 0) {
                result = 1.0 / (result * (-y));
            } else {
                for (int i = 1; i < y; i++) {
                    result = result * x;
                }
            }
                return result;
        }
    }
}

