package ru.bad.cat;

public class Cat {
    private String name;
    private int year;
    private Gender gender;

    public Cat(String name, Gender gender,int year ) {
        this.name = name;
        this.gender = gender;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }
    @Override
    public String toString() {
        return "(" +
                " Кличка - " + name + '\'' +
                ", Куратор -" + year +
                ", Пол -" + gender +
                ')';
    }

    public Gender getGender() {
        return gender;
    }

}
