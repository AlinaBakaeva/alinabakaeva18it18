package ru.bad.patient;

/**
 * Класс для представления пациента
 *
 * @author Bakaeva A.
 */
@SuppressWarnings("all")

public class Patient {

    /**
     * Фамилия пациента
     * Год рождения пациента
     * Номер карточки пациента
     * Диспанцеризация пациента
     */

    private String name;
    private int birthday;
    private int card;
    private boolean dispensary;

    /**
     * @param surname
     * @param birthday
     * @param card
     * @param dispensary
     */

    public Patient(String name, int birthday, int card, boolean dispensary) {
        this.name = name;
        this.birthday = birthday;
        this.card = card;
        this.dispensary = dispensary;
    }

    public Patient(){
        this("Не указана",0,0,false);
    }

    String getName() {
        return name;
    }

    int getBirthday() {
        return birthday;
    }

    int getCard() {
        return card;
    }

    boolean getDispensary() {
        return dispensary;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая фамилию, год рождения, номер карточки и информацию о диспанцеризации
     */

    @Override
    public String toString() {
        return "Patient{" +
                "Фамилия пациента - '" + name + '\'' +
                ", день рождения - '" + birthday + '\'' +
                ", номерт карты - " + card +
                ", диспанцеризация - " + dispensary +
                '}';
    }
}

