package ru.bad.patient;

public class PatientDemo {

    /**
     * Класс для работы с объектами класса Patient
     *
     * @author Bakaeva A.
     */

    public static void main(String[] args) {
        Patient patient1 = new Patient("Плюшков", 1998, 143, true);
        Patient patient2 = new Patient("Буданкин", 2000, 213, false);
        Patient patient3 = new Patient("Дьякова", 2001, 765, true);
        Patient patient4 = new Patient("Романова", 2000, 583, true);
        Patient patient5 = new Patient("Зайкин", 2000, 597, false);

        Patient[] patients = {patient1, patient2, patient3, patient4, patient5};
        dispPat(patients);
    }

    /**
     * Ищет и выводит массив пациентов, прошедших диспанцеризацию до 2000 года
     *
     * @param patients массив пациентов
     */

    private static void dispPat(Patient[] patients) {
        System.out.println("Пациенты: ");
        for (Patient patient : patients) {
            if (patient.getDispensary() && patient.getBirthday() < 2000) {
                System.out.println("Пациент{" +
                        "Фамилия='" + patient.getName() + '\'' +
                        ", Год рождения=" + patient.getBirthday() +
                        ", Номер карточки=" + patient.getCard() +
                        ", Диспанцеризация пройдена" +
                        '}');
            }
        }
    }
}

