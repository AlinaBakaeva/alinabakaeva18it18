package ru.bad.baggage;

/**
 * класс для представления багажа
 *
 * @author Бакаева А.
 */

public class Baggage {

    /**
     * Фамилия
     * Сколько мест для одного человека
     * Вес багажа
     */

    private String name;
    private int place;
    private double weight;

    /**
     * @param name
     * @param place
     * @param weight
     */

    Baggage(String name, int place, double weight) {
        this.name = name;
        this.place = place;
        this.weight = weight;
    }

    String getName() {
        return name;
    }

    private int getPlace() {
        return place;
    }

    void setPlace(int place) {
        this.place = place;
    }

    double getWeight() {
        return weight;
    }

    /**
     * Возвращает строковое представление объекта
     *
     * @return строка, содержащая фамилию, место и вес
     */

    @Override
    public String toString() {
        return "Baggage{" +
                "name='" + name + '\'' +
                ", place=" + place +
                ", weight=" + weight +
                '}';
    }

    /**
     * Метод, который позволяет определить относится багаж к ручной клади или нет
     */

    boolean handBaggage() {
        boolean alina = false;
            if (place == 1 && weight <= 10) {
                alina = true;
            }
            return alina;
        }
    }
