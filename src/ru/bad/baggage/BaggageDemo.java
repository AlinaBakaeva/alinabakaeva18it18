package ru.bad.baggage;

public class BaggageDemo {
    public static void main(String[] args) {

        /**
         * Класс для работы с объектами класса Baggage
         *
         * @author Bakaeva A.
         */

        Baggage baggage1 = new Baggage("Кошкин", 2, 5);
        Baggage baggage2 = new Baggage("Бакаева", 1, 12);
        Baggage baggage3 = new Baggage("Мерзляков", 1, 3);

        Baggage[] bagage = {baggage1, baggage2, baggage3};
        handBagage(bagage);
    }

    /**
     * Метод, позволяющий вывести на экран фамилии пассажиров с ручной кладью
     *
     * @param bagage массив пациентов
     */

    private static void handBagage(Baggage[] bagage) {
        for (Baggage baggage : bagage) {
            if (baggage.handBaggage()) {
                System.out.println(baggage.getName());

            }
        }
    }
}
