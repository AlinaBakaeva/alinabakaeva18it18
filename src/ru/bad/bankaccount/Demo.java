package ru.bad.bankaccount;
/**Класс для реализации банковского аккаунта
 *
 * @Autor Bakaeva A.D.
 */
public class Demo {
    public static void main(String[] args) {
            BankAccount bank = new BankAccount(834.5, 10.0);
            System.out.println(bank);
            System.out.print("Состояние банковского счёта с учетом процентных начислений за один год: " + bank.count());

        }

    }
