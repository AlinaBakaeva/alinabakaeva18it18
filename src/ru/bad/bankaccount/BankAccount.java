package ru.bad.bankaccount;

public class BankAccount {
    double balance;
    double interestRate;

    public BankAccount(double balance, double interestRate) {
        this.balance = balance;
        this.interestRate = interestRate;
    }

    /**
     * метод считает начисление процентов за год
     *
     * @return состояние банковского счета с учетом процентных начислений
     */

    public double count() {
        double balance1 = balance / 100;
        double balanceWithInterestRate = balance + (balance1 * interestRate);
        return balanceWithInterestRate;
    }

    @Override
    public String toString() {
        return "Bank{" +
                " Баланс =" + balance +
                ", Процентная ставка =" + interestRate +
                '}';
    }
}
